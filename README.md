# HDCore - docker-shellcheck

## Introduction

This is a small container image that contains shellcheck with a customized **shellcheck-dir&#46;sh** script. With this script you can give a directoryname instead of every individual file to the shellscript command. This container is very usefull for automatic testing during CI.

## Image usage

- Run shellcheck-dir&#46;sh:

```bash
docker run --rm -v /path/to/shell/code:/code hdcore/docker-shellcheck:<version> shellcheck-dir.sh <directory> [<extra-shellcheck-arguments>]
```

- Run shell:

```bash
docker run -it --rm -v /path/to/shell/code:/code hdcore/docker-shellcheck:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-shellcheck:<version>
script: shellcheck-dir.sh <directory> [<extra-shellcheck-arguments>]
```

## Arguments

Except for the of the \<directory\> argument (which must exist), all other arguments are documented on https://github.com/koalaman/shellcheck

## Available tags

- hdcore/docker-shellcheck:stable

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-shellcheck
  - registry.hub.docker.com/hdcore/docker-shellcheck
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-shellcheck

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-shellcheck:<version> .
```
