#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2021-12-03 18:04

# docker-entrypoint.sh
# for hdcore docker-shellcheck image

# Check parameters
if [ -z "${1}" ]
then
    SCANDIR="."
else
    SCANDIR="${1}"
fi

if [ ! -d "${SCANDIR}" ]
then
    echo "Error: directory \"${SCANDIR}\" does not exits!"
    exit 99
fi

shift

# Search files
TMPFILE="/tmp/shellfiles.txt"
:> "${TMPFILE}"

# Shell scripts
find "${SCANDIR}" -type f -name "*.sh" >> "${TMPFILE}"
# Githooks
find "${SCANDIR}" -type f -name "pre-commit" >> "${TMPFILE}"

# Remove special folders
sed -i -r "/.*\/(\.git|brol|venv|vendor|__pycache__|tmp)\/.*/d" "${TMPFILE}"

echo "Checking files:"
cat "${TMPFILE}"

# Execute shellcheck
# shellcheck disable=SC2068 # double quote not allowed due to extra parameter expansion
tr '\n' '\0' < "${TMPFILE}" | xargs -r -0 shellcheck $@
