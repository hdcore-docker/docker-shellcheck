#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2021-11-17 22:00

# docker-entrypoint.sh
# for hdcore docker-shellcheck image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start entrypoint ${0}${CNORMAL} ==\n"

printf "HDCore docker-shellcheck container image\n"
printf "Installed version of shellcheck:\n"
shellcheck --version

# shellcheck disable=SC2059
printf "== ${CGREEN}End entrypoint ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"